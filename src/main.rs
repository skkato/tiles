struct Games {
	games: std::sync::Mutex<std::collections::HashMap<
		String,
		std::sync::Arc<std::sync::Mutex<Room>>>>,
}

impl Games {
	fn make_game(&self) -> std::sync::Arc<std::sync::Mutex<Room>> {
		std::sync::Arc::new(std::sync::Mutex::new(Room::new()))
	}
	
	fn find_or_create(&self, id: String) -> std::sync::Arc<std::sync::Mutex<Room>> {
		let game = self.games
			.lock().unwrap()
			.entry(id)
			.or_insert_with(|| self.make_game()).clone();
		
		game
	}
	
	fn clean(&self) {
		let cutoff = std::time::Instant::now() - std::time::Duration::from_secs(3600);
		
		println!("Cleaning.");

		let mut games = self.games.lock().unwrap();
		let prev = games.len();
		games.retain(|_id, game| game.lock().unwrap().lastused > cutoff);

		println!("Removed {}/{} games.", games.len(), prev);
	}
}

struct Room {
	lastused: std::time::Instant,
	
	game: tiles::Game,
	connections: Vec<ws::Sender>,
}

impl Room {
	fn new() -> Self {
		Room {
			lastused: std::time::Instant::now(),
			game: tiles::Game::new(Default::default()),
			connections: Vec::new(),
		}
	}
	
	fn heartbeat(&mut self) {
		self.lastused = std::time::Instant::now();
	}
	
	fn add_connection(&mut self, ws: ws::Sender) {
		self.heartbeat();

		ws.send(bincode::serialize(&tiles::net::EventServer::New(self.game.clone())).unwrap()).unwrap();

		self.connections.push(ws);
	}

	fn broadcast(&mut self, event: &tiles::net::EventServer) {
		self.connections.retain(|ws| {
			match ws.send(bincode::serialize(event).unwrap()) {
				Ok(()) => true,
				Err(e) => {
					eprintln!("Connection unhealthy: {:?}", e);
					false
				}
			}
		});
	}
	
	fn click(&mut self, path: tiles::TilePath) {
		self.heartbeat();

		self.game.click(path);

		for m in self.game.mutations.split_off(0) {
			self.broadcast(&tiles::net::EventServer::Mutation(m));
		}
	}
}

struct ConnectionHandler<'s> {
	games: &'s Games,
	ws: ws::Sender,
	room: Option<std::sync::Arc<std::sync::Mutex<Room>>>,
}

impl<'a> ConnectionHandler<'a> {
	fn dispatch(&mut self, event: tiles::net::EventClient) {
		eprintln!("Event {:?}", event);
		let mut room = self.room.as_ref().unwrap().lock().unwrap();
		match event {
			tiles::net::EventClient::Click(path) => room.click(path),
			tiles::net::EventClient::Desync => {
				self.ws.send(bincode::serialize(
					&tiles::net::EventServer::New(room.game.clone())).unwrap()).unwrap();
			}
		}
	}
}

impl<'a> ws::Handler for ConnectionHandler<'a> {
	fn on_request(&mut self, req: &ws::Request) -> ws::Result<ws::Response> {
		let mut path = req.resource().split("/");
		assert_eq!(path.next(), Some(""));
		match path.next() {
			Some("game") => {
				let mut res = ws::Response::from_request(req)?;
				res.set_protocol(tiles::net::PROTOCOL);
				
				let id = path.next().unwrap_or_default();
				let room = self.games.find_or_create(id.into());
				room.lock().unwrap().add_connection(self.ws.clone());
				self.room = Some(room);
				
				Ok(res)
			},
			Some("ping") => {
				Ok(ws::Response::new(200, "OK", "d687629c-adc6-44c9-a817-5d30282f1514".into()))
			},
			_ => {
				Ok(ws::Response::new(404, "NOT FOUND", "404 Not Found".into()))
			},
		}
	}
	
	fn on_open(&mut self, _: ws::Handshake) -> ws::Result<()> {
		Ok(())
	}
	
	fn on_message(&mut self, msg: ws::Message) -> ws::Result<()> {
		let event = match msg {
			ws::Message::Text(s) => serde_json::from_str(&s).unwrap(),
			ws::Message::Binary(s) => bincode::deserialize(&s).unwrap(),
		};

		println!("Got message: {:?}", event);
		
		self.dispatch(event);
		Ok(())
	}
}

fn env(k: &str, default: &str) -> String {
	match std::env::var(k) {
		Ok(s) => s,
		Err(std::env::VarError::NotPresent) => default.into(),
		Err(std::env::VarError::NotUnicode(_)) => {
			panic!("The environment variable {:?} isn't valid UTF8", k)
		},
	}
}

fn main() {
	let bind = env("TILES_BIND", "127.0.0.1:8080");
	
	let games = Games{
		games: Default::default(),
	};

	let _cleanup = {
		let games = unsafe { std::mem::transmute::<&_, &'static Games>(&games) };
		std::thread::spawn(move || {
			std::thread::sleep(std::time::Duration::from_secs(3600));
			games.clean();
		})
	};

	let ws = ws::Builder::new()
		.build(|ws| ConnectionHandler{
			ws,
			games: &games,
			room: None,
		}).unwrap();

	println!("Listening at http://{}", bind);
	ws.listen(bind.as_str()).unwrap();
}
