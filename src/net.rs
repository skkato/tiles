pub const PROTOCOL: &str = "ca.kevincox.tiles.v1";

#[derive(Debug,serde_derive::Deserialize,serde_derive::Serialize)]
pub enum EventClient {
	Click(crate::TilePath),
	Desync,
}

#[derive(Debug,serde_derive::Deserialize,serde_derive::Serialize)]
pub enum EventServer {
	Mutation(crate::Mutation),
	New(crate::Game<rand_pcg::Pcg32>),
}

